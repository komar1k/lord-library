export interface BookModel {
  docs: IdentityBookModel[];
  total: number;
  limit: number;
  offset: number;
  page: number;
  pages: number;
}

export interface IdentityBookModel {
  _id: string;
  name: string;
}

export interface MoviesModel {
  docs: IdentityMovieModel[];
  total: number;
  limit: number;
  offset: number;
  page: number;
  pages: number;
}

export interface IdentityMovieModel {
  _id: string;
  name: string;
  runtimeInMinutes: number;
  budgetInMillions: number;
  boxOfficeRevenueInMillions: number;
  academyAwardNominations: number;
  academyAwardWins: number;
  rottenTomatesScore: number;
}

export interface BookChapterModel {
  docs: IdentityBookChapterModel[];
  total: number;
  limit: number;
  offset: number;
  page: number;
  pages: number;
}

export interface IdentityBookChapterModel {
  _id: string;
  bookName: string;
  chapterName: string;
}

export interface CharacterModel {
  docs: IdentityCharacterModel[];
  total: number;
  limit: number;
  offset: number;
  page: number;
  pages: number;
}

export interface IdentityCharacterModel {
  _id: string;
  birth: string;
  death: string;
  hair: string;
  gender: string;
  height: string;
  realm: string;
  spouse: string;
  name: string;
  race: string;
  wikiUrl: string;
}

export interface QuoteModel {
  docs: IdentityQuoteModel[];
  total: number;
  limit: number;
  offset: number;
  page: number;
  pages: number;
}

export interface IdentityQuoteModel {
  _id: string;
  dialog: string;
  movie: string;
  character: string;
}

export interface ChapterModel {
  docs: IdentityChapterModel[];
  total: number;
  limit: number;
  offset: number;
  page: number;
  pages: number;
}

export interface IdentityChapterModel {
  _id: string;
  chapterName: string;
  book: string;
}
