import { NgModule } from '@angular/core';
import { LordLibComponent } from './lord-lib.component';



@NgModule({
  declarations: [LordLibComponent],
  imports: [
  ],
  exports: [LordLibComponent]
})
export class LordLibModule { }
