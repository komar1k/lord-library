import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BookChapterModel, BookModel, ChapterModel, CharacterModel, MoviesModel, QuoteModel} from './data';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LordLibService {

  constructor(private http: HttpClient) {
  }

  public url = 'https://the-one-api.dev/v2';

  public getBooks(): Observable<BookModel> {
    return this.http.get<BookModel>(`${this.url}/book`);
  }

  public getSpecificBook(id: string): Observable<BookModel> {
    return this.http.get<BookModel>(`${this.url}/book/${id}`);
  }

  public getAllChaptersOfASpecificBook(id: string): Observable<BookChapterModel> {
    return this.http.get<BookChapterModel>(`${this.url}/book/${id}/chapter`);
  }

  public getListOfMovies(): Observable<MoviesModel> {
    return this.http.get<MoviesModel>(`${this.url}/movie`);
  }

  public getSpecificMovie(id: string): Observable<MoviesModel> {
    return this.http.get<MoviesModel>(`${this.url}/movie/${id}`);
  }

  public getListOfCharacters(): Observable<CharacterModel> {
    return this.http.get<CharacterModel>(`${this.url}/character`);
  }

  public getSpecificCharacter(id: string): Observable<CharacterModel> {
    return this.http.get<CharacterModel>(`${this.url}/character/${id}`);
  }

  public getQuotes(): Observable<QuoteModel> {
    return this.http.get<QuoteModel>(`${this.url}/quote`);
  }

  public getSpecificMovieQuote(id: string): Observable<QuoteModel> {
    return this.http.get<QuoteModel>(`${this.url}/quote/${id}`);
  }

  public getChapters(): Observable<ChapterModel> {
    return this.http.get<ChapterModel>(`${this.url}/chapter`);
  }

  public getSpecificChapter(id: string): Observable<ChapterModel> {
    return this.http.get<ChapterModel>(`${this.url}/chapter/${id}`);
  }
}
